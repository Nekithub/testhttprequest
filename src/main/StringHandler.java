package main;

/*
 * Класс предназначенный для поиска нужных данных из какого то буфера.
 * @author Глухов Никита
 * @version 1.0
 */
public class StringHandler {

    /* Константа для интервала между графой */
    private final int INTERVAL_BETWEEN = 3;

    /* Поле информации которую надо обработать */
    private String originalString;

    /*
     * Конструктор - создание нового объекта
     * @param inputArg - информация для обработки
     */
    public StringHandler (String inputArg){
        originalString = inputArg;
    }

    /*
     * Метод поиска информации по названию графы
     * @param search - название графы по которой ищем
     * @return возвращает найденную строку
     */
    public String getSearchInf(String search){

        // Узнаем с какого индекса начинается наша информация
        int indexStart = originalString.indexOf(search);

        // складываем размер названия графы
        indexStart += search.length();

        // складываем интервал
        indexStart += INTERVAL_BETWEEN;

        // получаем конечный индекс для обрезки строки
        int indexEnd = originalString.indexOf( '"',indexStart);

        // и затем возвращаем ее
        return originalString.substring(indexStart,indexEnd);
    }

}



