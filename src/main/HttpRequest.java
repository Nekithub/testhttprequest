package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import main.StringHandler;

/*
 * Класс служит для выполнения запроса на сервис reqres и получения ответа
 * @author Глухов Никита
 * @version 1.1
 */
public class HttpRequest {

    // мой "define"
    public static final String INPUTREQUEST = "https://reqres.in/api/users/";

    /*
    * Главный метод программы, получает ответ с ресурса по запросу константы INPUTREQUEST
    * @params args - параметр командной строки передаваемый пользователем
    */
    public static void main(String[] args) {
        try {

            // Тут складывается наш запрос, сюда же вместо единицы нужно присоединять строку.
            URL getUrl = new URL(INPUTREQUEST + args[0]);

            HttpURLConnection getUrlConnection = (HttpURLConnection) getUrl.openConnection();

            // создаем поток вывода для обработки ответа
            BufferedReader inputData = new BufferedReader(
                    new InputStreamReader(getUrlConnection.getInputStream()));

            // строку где у нас будет лежать информация
            String inputLine;

            // передаем в нее информацию
            inputLine = inputData.readLine();

            // содаем объект для обработки и получения желаемой информаций
            StringHandler handler = new StringHandler(inputLine);

            // передаем и получаем аргумент с графы first_name
            System.out.print(handler.getSearchInf("first_name") + ' ');

            // передаем и получаем аргумент с графы last_name
            System.out.print(handler.getSearchInf("last_name"));

            // удаляем inputData
            inputData.close();
        } catch (IOException e) {

            // если произошла ошибка, т.е. вызвалось исключение
            System.out.print("User not found!");
        }
    }
}
